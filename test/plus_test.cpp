#include <gtest/gtest.h>
#include <foo.h>

TEST(plus, plus_positive_works) {
    ASSERT_EQ(3, plus(2, 1));
}

TEST(plus, plus_negative_works) {
    ASSERT_EQ(-5, plus(-1, -4));
}

TEST(plus, plus_zero_does_nothing) {
    ASSERT_EQ(-4, plus(0, -4));
}
