#include <gtest/gtest.h>
#include <foo.h>

TEST(minus, minus_negative_works)
{
	ASSERT_EQ(0, minus(-1, -1));
}

TEST(minus, minus_positive_works)
{
	ASSERT_EQ(0, minus(1, 1));
}
