# gtest example project

It is a simple project using [google-test](https://github.com/google/googletest)
library with [cmake](https://cmake.org/). It was created mainly to demonstrate
Google Test template for KDevelop. I hope to integrate it soon. Here is a
screenshot:

![kdev-gtest](images/kdevelop-gtest.png)

## Build instructions

* Fedora 23

Fedora 23 has a pretty new cmake version (3.4.1 at the moment of writing), which
has a nice support of Google Test. With it, you can throw away some boilerplate
from CMakeLists.txt.

Remove:

        link_directories($ENV{GTEST_ROOT})

Remove `pthread` from this line:

        target_link_libraries(foo pthread ${GTEST_BOTH_LIBRARIES})

Change:

        GTEST_ADD_TESTS(foo "" test/minus_test.cpp)
        GTEST_ADD_TESTS(foo "" test/plus_test.cpp)

to:

        GTEST_ADD_TESTS(foo "" AUTO)

Install dependencies:

        # dnf install cmake make g++ gtest-devel

Build:

        mkdir build
        cd build
        cmake ..
        make

* Debian 8 Jessie

On Debian Jessie, you will need to build gtest manually and then link project
against it.

Install dependencies:

        # apt install cmake cmake-data make g++ libgtest-dev


Build:

        mkdir build
        cd build
        mkdir gtest
        cd gtest
        cmake /usr/src/gtest/
        make
        cd ..
        GTEST_ROOT=$PWD/gtest cmake ..
        make
